// grab the inputs
const inputs = document.querySelectorAll('.controls input');
const root = document.documentElement;

function inputHandler(e) {
    //the new value: 
    const newValue = e.currentTarget.value;
    const elementId = e.currentTarget.id;
    const unit = e.currentTarget.dataset.sizing || '';
    // update the css variables with the new value
    root.style.setProperty(`--${elementId}`, `${newValue}${unit}`);
}

inputs.forEach( i => i.addEventListener('input', inputHandler));