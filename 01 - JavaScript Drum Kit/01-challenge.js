
//my attempt:
//const keyInputs = Array.from(document.querySelectorAll('.key'));
// const audios = Array.from(document.querySelectorAll('audio'))
// const keys = ['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l'];

// function keyHandler(event){
//     // 1. detect if the one pressed is a valid key
//     const validKey = keys.find(key => key === event.key);
//     if(!validKey){ return; }
//     // 2. add the played class to the correct element field
//     const pressedKey = keyInputs.find(keyInput => keyInput.getAttribute('data-key') == event.keyCode);
//     const sound = audios.find(audio => audio.getAttribute('data-key') == event.keyCode);
//     pressedKey.classList.add('playing');
//     sound.currentTime = 0;
//     sound.play();
//     setTimeout(()=> pressedKey.classList.remove('playing'), 100);
// }

//window.addEventListener('keydown', keyHandler);

// ############################################################################################3333
//how wes bos solved it:
//what I liked more about his solution:
// 1. the way he selects only the needed sound and key
// 2. the transitionend event listener instead of the timeout
const keyInputs = Array.from(document.querySelectorAll('.key'));

function playSound(e) {
    const sound = document.querySelector(`audio[data-key="${e.keyCode}"]`);
    const key = document.querySelector(`.key[data-key="${e.keyCode}"]`);
    if(!sound) {return;}
    sound.currentTime = 0;
    sound.play();
    key.classList.add('playing');
}

function removeTransition(e) {
    //this is what the function is called against, and in this case, it's a key.
    this.classList.remove('playing');
}

window.addEventListener('keydown', playSound);
keyInputs.forEach(key => key.addEventListener('transitionend', removeTransition))