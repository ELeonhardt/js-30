console.log('it works!');
const canvas = document.querySelector('canvas');
const context = canvas.getContext('2d');

let mousedown = false;
let colorValue = 50;
context.strokeStyle = `hsl(${colorValue}, 100%, 50%)`;
context.lineWidth = 8;

function positionCursor(x, y) {
    context.beginPath();
    context.moveTo(x, y);
}

function draw(x, y) {
    context.lineTo(x, y);
    context.stroke();
    colorValue++;
    context.strokeStyle = `hsl(${colorValue}, 100%, 50%)`;
    context.beginPath();
    context.moveTo(x, y);
}

function mouseDownHandler(e) {
    mousedown = true;
    context.moveTo(e.clientX, e.clientY);
}

function mouseUpHandler(e) {
    mousedown = false;
}

function moveHandler(e) {
    if(!mousedown) {
        return;
    }
    draw(e.clientX, e.clientY);
}

canvas.addEventListener('mousemove', moveHandler);
canvas.addEventListener('mousedown', mouseDownHandler);
canvas.addEventListener('mouseup', mouseUpHandler);

