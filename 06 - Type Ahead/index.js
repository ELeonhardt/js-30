const endpoint = 'https://gist.githubusercontent.com/Miserlou/c5cd8364bf9b2420bb29/raw/2bf258763cdddd704f8ffd3ea9a3e81d25e2c6f6/cities.json';
let cities = [];
const dataInput = document.querySelector('.search-form input');
const suggestions = document.querySelector('.search-form .suggestions');


async function fetchData() {
    const response = await fetch(endpoint);
    const data = await response.json();
    cities = Array.from(data);
    console.log(cities);
}

async function go() {
    await fetchData();
}

function inputFilter(array, expression) {
    const regex = new RegExp(expression, 'gi');
    const subset = array.filter(item => item.city.match(regex) || item.state.match(regex));
    const html = subset.map(item => {
        const population = parseInt(item.population).toLocaleString('en-US');
        const cityHl = item.city.replace(regex, `<span class="hl">${expression}</span>`);
        console.log(cityHl);
        const stateHl = item.state.replace(regex, `<span class="hl">${expression}</span>`);
        console.log(stateHl);
        return `<li><span class="name">${cityHl}, ${stateHl}</span><span class="population">${population}</span></li>`

    }).join('');
    suggestions.innerHTML = html;
    // console.log(html);

}

//filters the array
function inputHandler(e) {
    console.log(e.target.value.length);
    if(e.target.value.length !== 0) {
        inputFilter(cities, e.target.value);
    }
    else {
        suggestions.innerHTML = `<li>Filter for a city</li>
        <li>or a state</li>`;
    }
}

dataInput.addEventListener('input', inputHandler);

go();
const string = "newyork";
const reg = new RegExp('new', 'gi');
console.log(string.match(reg));
// function inputHandler(e) {
//     // console.log(e.target.value);
//     // const matches = cities.filter(city => city.city.includes(e.target.value)).map(match => {
//     //     return `<li>${match.city}, ${match.state} <span class="population">${match.population}</span></li>`;
//     // }).join('');
//     // suggestions.innerHTML = matches;
//     // 1. capitalize the user input
//     const upperCased = e.target.value.split(' ')
//         .map(item => {
//             if(item){
//                 return `${item[0].toUpperCase()}${item.slice(1)}`;
//             }})
//         .join(' ');
//     const matches = cities.filter(city => (city.city.includes(upperCased) || city.state.includes(upperCased)));
//     console.log(matches);
//     const forDisplay = matches.map(match => {
//         const number = parseInt(match.population).toLocaleString();
//         return `<li>${match.city}, ${match.state} <span class="population">${number}</span></li>`;
//     }).join('');
//     suggestions.innerHTML= forDisplay;

// }

// dataInput.addEventListener('input', inputHandler);
// go();

// const text = "new york ";
// const splitted = text.split(' ').map(item => {
//     if(item){
//         return `${item[0].toUpperCase()}${item.slice(1)}`;
//     }}).join(' ');
// console.log(splitted);
// // const mapped = splitted.map(item => {
// //     if(item){
// //         return `${item[0].toUpperCase()}${item.slice(1)}`;
// //     }});
// // console.log(mapped.join(' '));
// // const finish = mapped.filter(item => item);
// // console.log(finish.join(' '));